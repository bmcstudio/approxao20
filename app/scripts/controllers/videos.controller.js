(function() {
	'use strict';

	/* jshint validthis: true */
	/**
	 * @ngdoc function
	 * @name appRoxao.controller:VideosCtrl
	 * @description
	 * # VideosCtrl
	 * Controller of the appRoxao
	 */
	angular.module('appRoxao')
		.controller('VideosCtrl', VideosCtrl);

	VideosCtrl.$inject = ['$http'];

	function VideosCtrl ($http) {
		var vm = this;

		vm.shareVideo = shareVideo;
		vm.openExternalLink = openExternalLink;

		$http.get('http://www.bmchost.com.br/bardez/appRoxao/Administrator/listvideos.json').then(doneCallbacks, failCallbacks);
		// $http.get('scripts/listvideos.json').then(doneCallbacks, failCallbacks);

		function doneCallbacks (response) {
			vm.listVideos = response.data.allVideos;
		}

		function failCallbacks (response) {
			navigator.notification.alert(
				'Não foi possível estabelecer conexão com o servidor. Por favor tente novamente mais tarde.',  // message
				null,         // callback
				'Atenção',    // title
				'OK'          // buttonName
			);
		}

		function shareVideo (link) {
			window.plugins.socialsharing.share('Dá o play e confira os melhores vídeos de Roxão', null, null, link);
		}

		function openExternalLink(link) {
			window.open(encodeURI(link), '_system', 'location=yes');
			return false;
		}

	}


})();
