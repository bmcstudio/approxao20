(function() {
	'use strict';

	/* jshint validthis: true */
	/**
	 * @ngdoc function
	 * @name appRoxao.controller:LeilaoCtrl
	 * @description
	 * # LeilaoCtrl
	 * Controller of the appRoxao
	 */
	angular.module('appRoxao')
		.controller('LeilaoCtrl', LeilaoCtrl);


	function LeilaoCtrl () {
		var vm = this;

		vm.openExternalLink = openExternalLink;

		function openExternalLink() {
			window.open(encodeURI('http://anadantasranch.com.br/leilao/flip/'), '_system', 'location=yes');
			return false;
		}

	}


})();
