(function() {
	'use strict';

	/* jshint validthis: true */
	/**
	 * @ngdoc function
	 * @name appRoxao.controller:FotosCtrl
	 * @description
	 * # FotosCtrl
	 * Controller of the appRoxao
	 */
	angular.module('appRoxao')
		.controller('FotosCtrl', FotosCtrl);

	FotosCtrl.$inject = ['$http'];

	function FotosCtrl ($http) {

		var vm = this,
			myModal = $('#myModal');

		vm.openPhoto = openPhoto;

		$http.get('http://www.bmchost.com.br/bardez/appRoxao/Administrator/listPhotos.json').then(doneCallbacks, failCallbacks);
		// $http.get('scripts/listPhotos.json').then(doneCallbacks, failCallbacks);

		function doneCallbacks (response) {
			vm.listPhotos = response.data.allPhotos;
		}

		function failCallbacks (response) {
			navigator.notification.alert(
				'Não foi possível estabelecer conexão com o servidor. Por favor tente novamente mais tarde.',  // message
				null,         // callback
				'Atenção',    // title
				'OK'          // buttonName
			);
		}

		function openPhoto (src, title) {

			vm.imgSrc = src;
			vm.imgTitle = title;

			myModal.modal();
		}

	}

})();
