(function() {
	'use strict';

	/* jshint validthis: true */
	/**
	 * @ngdoc function
	 * @name appRoxao.controller:MainCtrl
	 * @description
	 * # MainCtrl
	 * Controller of the appRoxao
	 */
	angular.module('appRoxao')
		.controller('MainCtrl', MainCtrl);

	MainCtrl.$inject = ['player', '$scope'];

	function MainCtrl (player, $scope) {

		var vm = this;

		// functions
		vm.play = player.play;
		vm.back = player.back;
		vm.next = player.next;
		vm.playing = player.playing;
		vm.curmusic = player.getCurrentMusic;
		vm.curperc = player.getCurperc();
		vm.shareApp = shareApp;
		vm.shareMusic = shareMusic;


		///////////////////////

		function shareApp () {
			window.plugins.socialsharing.share('Message, image and link', null, 'images/bg.jpg', null);
		}

		function shareMusic () {
			var music = player.getCurrentMusic();
			window.plugins.socialsharing.share('Você está ouvindo a trilha de sucesso de Roxão: ', null, music.cover, music.audio);
		}


	}


})();
