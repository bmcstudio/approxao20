(function() {
	'use strict';

	angular.module('services', []);
	angular.module('directives', []);

	/**
	 * @ngdoc overview
	 * @name appRoxao
	 * @description
	 * # appRoxao
	 *
	 * Main module of the application.
	 */
	angular
		.module('appRoxao', [
			'ngAnimate',
			'ngCookies',
			'ngResource',
			'ngRoute',
			'ngSanitize',
			'ngTouch',
			'services',
			'directives'
		])
		.run(startApp)
		.config(configApp);

	startApp.$inject = ['$rootScope', '$window'];

	function startApp ($rootScope, $window) {
		// publish current transition direction on rootScope
		$rootScope.direction = 'ltr';
		// listen change start events
		$rootScope.$on('$routeChangeStart', function(event, next, current) {
			$rootScope.direction = 'rtl';
			// console.log(arguments);
			if (current && next && (current.depth > next.depth)) {
				$rootScope.direction = 'ltr';
			}
			// back
			$rootScope.back = function() {
				$window.history.back();
			};
  		});
	}

	configApp.$inject = ['$routeProvider', '$httpProvider'];

	function configApp ($routeProvider, $httpProvider) {

		//Enable cross domain calls
		$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];

		$routeProvider
			.when('/', {
				templateUrl: 'views/main.html',
				controllerAs: 'vm',
				depth: 1,
				controller: 'MainCtrl'
			})
			.when('/historia', {
				templateUrl: 'views/historia.html',
				controllerAs: 'vm',
				depth: 2,
			})
			.when('/producao', {
				templateUrl: 'views/producao.html',
				controllerAs: 'vm',
				depth: 2,
			})
			.when('/premiacao', {
				templateUrl: 'views/premiacao.html',
				controllerAs: 'vm',
				depth: 2,
			})
			.when('/fotos', {
				templateUrl: 'views/fotos.html',
				controllerAs: 'vm',
				depth: 2,
				controller: 'FotosCtrl'
			})
			.when('/videos', {
				templateUrl: 'views/videos.html',
				controllerAs: 'vm',
				depth: 2,
				controller: 'VideosCtrl'
			})
			.when('/leilao', {
				templateUrl: 'views/leilao.html',
				controllerAs: 'vm',
				depth: 2,
				controller: 'LeilaoCtrl'
			})
			.when('/leilao/entrevista', {
				templateUrl: 'views/entrevista.html',
				controllerAs: 'vm',
				depth: 3
			})
			.when('/leilao/lotes', {
				templateUrl: 'views/lotes.html',
				controllerAs: 'vm',
				depth: 3
			})
			.when('/leilao/regulamento', {
				templateUrl: 'views/regulamento.html',
				controllerAs: 'vm',
				depth: 3
			})
			.when('/leilao/parceiros', {
				templateUrl: 'views/parceiros.html',
				controllerAs: 'vm',
				depth: 3
			})
			.when('/leilao/contato', {
				templateUrl: 'views/contato.html',
				controllerAs: 'vm',
				depth: 3
			})
			.otherwise({
				redirectTo: '/'
			});
	}

})();
