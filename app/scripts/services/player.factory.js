(function(){

    'use strict';

    /* jshint validthis: true */
    /**
    * @ngdoc function
    * @name appRoxao.controller:player
    * @description
    * # player
    * Factory of the appRoxao
    */
    angular.module('services')
        .factory('player', player);

    player.$inject = ['$http'];

    function player($http) {

        $http.get('http://www.bmchost.com.br/bardez/appRoxao/Administrator/listMusics.json').then(doneCallbacks, failCallbacks);
        // $http.get('scripts/listMusics.json').then(doneCallbacks, failCallbacks);

        var curmusic = {},
            song = null,
            itsPlay = false,
            curperc = 0,
            curtime = 0,
            count = 0;

        var service = {
            listMusics: [],
            song: null,
            play: play,
            back: back,
            next: next,
            stop: stop,
            playing: playing,
            getCurperc: getCurperc,
            getCurrentMusic: getCurrentMusic
        };

        return service;

        ////////////

        function doneCallbacks (response) {

            service.listMusics = response.data.allMusics;
            angular.copy(service.listMusics[0], curmusic);

            addEventSong();
        }

        function failCallbacks (response) {
            navigator.notification.alert(
                'Não foi possível estabelecer conexão com o servidor. Por favor tente novamente mais tarde.',  // message
                null,         // callback
                'Atenção',    // title
                'OK'          // buttonName
            );

            // object error
            curmusic = {
                title: 'Erro ao conectar com o servidor',
                cover: 'images/img_indisponivel.jpg'
            }
        }

        function playing () {
            return itsPlay;
        }

        function getCurrentMusic () {
            return curmusic;
        }

        function getCurperc () {
            return curperc;
        }

        function play () {

            if (itsPlay)
                song.pause();
            else
                song.play();

            itsPlay = !itsPlay;
        }

        function back () {

            if (song.readyState != 0) {

                count--;
                stop();

                if (count < 0) count = service.listMusics.length -1;

                curmusic = {};
                angular.copy(service.listMusics[count], curmusic);
                addEventSong();
            }

        }

        function next () {

            if (song.readyState != 0) {

                count++;
                stop();

                if (count > service.listMusics.length -1) count = 0;

                curmusic = {};
                angular.copy(service.listMusics[count], curmusic);
                addEventSong();
            }

        }

        function stop () {
            itsPlay = false;
            rmEventSong();
        }

        function addEventSong () {

            song = new Audio(curmusic.audio);

            song.addEventListener('timeupdate', function () {
                curtime = parseInt(song.currentTime, 10);
                curperc = (100 * curtime) / song.duration;

                if (document.getElementById('seek') !== null)
                    document.getElementById('seek').style.width = curperc + '%';

            });

            song.addEventListener('ended', function() {

                itsPlay = false;
                $scope.$apply();

                song.currentTime = 0;
            });
        }

        function rmEventSong () {
            song.pause();
            if (song.readyState != 0) song.currentTime = 0;
            song.removeEventListener('timeupdate');
            song.removeEventListener('ended');
            document.getElementById('seek').style.width = '0%';
        }

    }

})();
